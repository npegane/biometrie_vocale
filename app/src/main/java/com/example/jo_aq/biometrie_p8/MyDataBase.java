package com.example.jo_aq.biometrie_p8;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static butterknife.internal.Utils.arrayOf;

public class MyDataBase extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Biometrie.db";
    public static final String TABLE_NAME = "Biom_user";
    public static final int DATABASE_VERSION = 2;
    public static final String COL_ID = "id";
    public static final String COL_NAME = "name";
    public static final String COL_EMAIL = "email";
    public static final String COL_PWD = "pwd";
    //
    private static final String TABLE2_NAME = "Pwd_manager";
    public static final String COL2_ID = "id";
    private static final String COL2_USER = "id_user";
    private static final String COL_APP = "name_app";
    private static final String COL_LOGIN = "login_app";
    private static final String COL2_PWD = "pwd_app";


    public MyDataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("CREATE TABLE Biom_user (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, email VARCHAR(256),pwd VARCHAR(25));");
        db.execSQL("CREATE TABLE Pwd_manager (id INTEGER PRIMARY KEY AUTOINCREMENT, id_user INTEGER, name_app VARCHAR(256), login_app VARCHAR(25), pwd_app VARCHAR(25))");
        Log.i( "DATABASE", "onCreate invoked" );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE2_NAME);

        Log.i( "DATABASE", "ONUPGRADE invoked" );
        onCreate(db);
    }
    public void insertUser(User user){
        ContentValues val = new ContentValues();

        val.put(COL_ID, user.getId());
        val.put(COL_NAME, user.getName());
        val.put(COL_EMAIL,user.getEmail());
        val.put(COL_PWD, user.getPassword());

        this.getWritableDatabase().insert(TABLE_NAME, null, val);
        Log.i("DATABASE", "insertUser realize");
    }

    public void deleteAllUser(){
        this.getWritableDatabase().delete(TABLE_NAME, null, null);
        Log.i("DATABASE", "deleteAllUser realize");
    }

    public void deleteUserById(String id){

        String[] args = arrayOf(id);

        this.getWritableDatabase().delete(TABLE_NAME, "id = ?", args);
        System.out.println(this.getAllUser().size());
        Log.i("DATABASE", "DELETION");
    }

    public void updateName(User user){
        ContentValues val = new ContentValues();

        val.put(COL_NAME, user.getName());

        this.getWritableDatabase().update(TABLE_NAME, val, "email ="+user.getEmail(),null);
        Log.i("DATABASE", "updateName realize");
    }

    public User getUser(String mail){
        Cursor c = this.getReadableDatabase().query(TABLE_NAME, new String[]{COL_NAME, COL_EMAIL, COL_PWD}, COL_EMAIL +"='"+ mail+"'", null, null, null, null);
        Log.i("DATABASE", "getUser realize");
        return cursorToUser(c);
    }

    public User cursorToUser(Cursor c){
        if(c.getCount() == 0){
            return null;
        }

        c.moveToFirst();
        User user = new User();
        user.setName(c.getString(0));
        user.setEmail(c.getString(1));
        user.setPassword(c.getString(2));

        c.close();
        return user;
    }

    public List<User> getAllUser(){
        List<User> allUsers = new ArrayList<>();

        String sql = "select id, name, email, pwd from Biom_user";
        Cursor cursor = this.getReadableDatabase().rawQuery(sql, null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            User user = new User();
            user.setId(cursor.getString(0));
            user.setName(cursor.getString(1));
            user.setEmail(cursor.getString(2));
            user.setPassword(cursor.getString(3));

            allUsers.add(user);
            cursor.moveToNext();
        }
        System.out.println(allUsers.size());
        cursor.close();
        Log.i("DATABASE", "getAllUser realize");
        return allUsers;
    }

    void insertAccount(AccountManager accountManager){
        ContentValues val = new ContentValues();

        val.put(COL2_USER, accountManager.getId_user());
        val.put(COL_APP, accountManager.getName_app());
        val.put(COL_LOGIN, accountManager.getLogin_app());
        val.put(COL2_PWD, accountManager.getPwd_app());

        this.getWritableDatabase().insert(TABLE2_NAME, null, val);
        Log.i("DATABASE", "Insert Account realize");
    }

    public void deleteAccount(int id){
        this.getWritableDatabase().delete(TABLE2_NAME, "id ="+id, null);
        Log.i("DATABASE", "deleteAccount realize");
    }

    public void updateAccount(AccountManager account){
        ContentValues val = new ContentValues();

        val.put(COL2_PWD, account.getPwd_app());

        this.getWritableDatabase().update(TABLE2_NAME, val, "id ="+account.getId(), null);
    }

    List<AccountManager> getAccountUser(String id_user){
        List<AccountManager> accountUser = new ArrayList<>();

        String sql = "select id, id_user, name_app, login_app, pwd_app from Pwd_manager where id_user ="+id_user;
        Cursor cursor = this.getReadableDatabase().rawQuery(sql, null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            AccountManager account = new AccountManager();
            account.setId(cursor.getString(0));
            account.setId_user(cursor.getString(1));
            account.setName_app(cursor.getString(2));
            account.setLogin_app(cursor.getString(3));
            account.setPwd_app(cursor.getString(4));
            accountUser.add(account);
            cursor.moveToNext();
        }
        cursor.close();
        Log.i("DATABASE", "getAccountUser realize");
        return accountUser;
    }



    public void closeConnection(){
        this.close();
    }
}
