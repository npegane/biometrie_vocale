package com.example.jo_aq.biometrie_p8;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import AlizeSpkRec.AlizeException;
import AlizeSpkRec.IdAlreadyExistsException;
import AlizeSpkRec.SimpleSpkDetSystem;

import static android.Manifest.permission.RECORD_AUDIO;

public class VerifyActivity extends AppCompatActivity {
    protected static final int RECORDER_SAMPLERATE = 8000;
    protected static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    protected static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    protected int bufferElements2Rec = 2000;
    protected int bytesPerElement = 2; // 2 bytes in 16bit format
    protected long startTime;
    protected TextView timeText;
    protected SpkRecSys demoSpkRecSystem;
    protected boolean emptyRecord = false;
    protected boolean recordExists = false;
    protected boolean recordError = false;
    protected AudioRecord recorder = null;
    protected Button startRecordButton, stopRecordButton;
    protected Thread recordingThread = null, addSamplesThread = null, timerThread = null;
    protected Locale defaultLanguage;
    protected TextView txtName, resultText;
    private String speakerId;
    private String speakerMail;
    private Button btnPassword;
    private TextView txtPassword;
    private User currentUser;
    private MyDataBase mdb;
    private Button btnConnexion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);
        Bundle extras = getIntent().getExtras();
        speakerId = extras.getString("SPEAKERID");
        speakerMail = extras.getString("SPEAKERMAIL");

        Log.i("VERIFY", "SPEAKERID: "+ speakerId);
        try {
            //Initialize the SpeakerRecognition system and get the singleton instance.
            demoSpkRecSystem = SpkRecSys.getSharedInstance(this);
        }
        catch (AlizeException | IOException e) {
            e.printStackTrace();
        }
        startRecordButton = findViewById(R.id.startRecordButton);
        stopRecordButton = findViewById(R.id.stopRecordButton);
        timeText = findViewById(R.id.timeText);
        //txtName = findViewById(R.id.txtName);
        resultText = findViewById(R.id.result_text);
        btnPassword = findViewById(R.id.btnPassword);
        btnConnexion = findViewById(R.id.btnConnexion);
        txtPassword = findViewById(R.id.txtPassword);

        startRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRecording();
            }
        });

        stopRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecording();
            }
        });
        btnPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mdb = new MyDataBase(getApplicationContext());
                currentUser= mdb.getUser(speakerMail);
                System.out.println(currentUser.getPassword().toString());
                System.out.println(txtPassword.getText().toString());
                System.out.println(currentUser.getPassword() == txtPassword.getText().toString());
                mdb.close();
                if(currentUser != null && currentUser.getPassword().equals(txtPassword.getText().toString())){
                    Intent i = new Intent(getApplicationContext(), ManagePwd.class);
                    i.putExtra("USERID", speakerId);
                    i.putExtra("USERMAIL", speakerMail);
                    startActivity(i);
                }
            }
        });

        btnConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent i = new Intent(getApplicationContext(), ManagePwd.class);
                i.putExtra("USERID", speakerId);
                i.putExtra("USERMAIL", speakerMail);
                startActivity(i);*/
            }
        });
    }

    protected boolean checkPermission() {
        return ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED;
    }

    protected void requestPermission() {
        ActivityCompat.requestPermissions(VerifyActivity.this, new
                String[]{RECORD_AUDIO}, 42);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 42: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startRecording();
                }
                else {
                    //makeToast("est");
                    System.out.println("ERREUR PERMISSION");
                }
                break;
            }
        }
    }

    protected void startRecording() {
        if (!checkPermission()) {
            requestPermission();
            return;
        }

        emptyRecord = true;
        recordError = false;
        startRecordButton.setVisibility(View.INVISIBLE);
        stopRecordButton.setVisibility(View.VISIBLE);
        timeText.setText("00:00:00");

        //Start the record
        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING, bufferElements2Rec * bytesPerElement);
        recorder.startRecording();

        if (recordExists) {
            try {
                //Reset input, since we will not make any more use of this audio signal.
                demoSpkRecSystem.resetAudio();       //Reset the audio samples of the Alize system.
                demoSpkRecSystem.resetFeatures();    //Reset the features of the Alize system.
            } catch (AlizeException e) {
                e.printStackTrace();
            }
            recordExists = false;
        }

        final List<short[]> audioPackets = Collections.synchronizedList(new ArrayList<short[]>());

        //This thread is meant to record the audio samples with android recorder.
        recordingThread = new Thread(new Runnable() {
            private Handler handler = new Handler();

            @Override
            public void run() {
                startTime = System.currentTimeMillis();

                short[] tmpAudioSamples = new short[bufferElements2Rec];
                while (recorder.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
                    int samplesRead = recorder.read(tmpAudioSamples, 0, bufferElements2Rec);

                    if (samplesRead > 0) {
                        short[] samples = new short[samplesRead];
                        //System.arraycopy(tmpAudioSamples, 0, samples, 0, samplesRead);
                        for (int i=0; i < samples.length; i++) {
                            samples[i] = tmpAudioSamples[i];
                            if (samples[i] != 0) {
                                emptyRecord = false;
                            }
                        }

                        synchronized (audioPackets) {
                            audioPackets.add(samples);
                        }
                    }
                }
            }
        }, "AudioRecorder Thread");

        //This thread is meant to use the audio samples and send them to the Alize system.
        addSamplesThread = new Thread(new Runnable() {
            private Handler handler = new Handler();

            @Override
            public void run() {
                short[] nextElement;

                while ((recorder.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING)
                        || (!audioPackets.isEmpty())) {
                    nextElement = null;

                    synchronized (audioPackets) {
                        if (!audioPackets.isEmpty()) {
                            nextElement = audioPackets.get(0);
                            audioPackets.remove(0);
                        }
                    }
                    if (nextElement != null) {
                        try {
                            //Receive an audio signal as 16-bit signed integer linear PCM, parameterize it and add it to the feature server.
                            demoSpkRecSystem.addAudio(nextElement);
                        } catch (AlizeException e) {
                            e.printStackTrace();
                            System.out.println("ERROR ALIZE EXCEPTION");
                            recordError = true;
                        } catch (Throwable e) { //TODO catch proper exception
                            System.out.println("ERROR THROWABLE EXCEPTION");
                            e.printStackTrace();
                            recordError = true;
                        }
                    }
                }

                try {
                    recordingThread.join(); //Wait the recordingThread to end.
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                while (!audioPackets.isEmpty()) {
                    nextElement = audioPackets.get(0);
                    audioPackets.remove(0);

                    if (nextElement != null) {
                        try {
                            demoSpkRecSystem.addAudio(nextElement);
                        } catch (AlizeException e) {
                            System.out.println("ERROR ALIZE EXCEPTION");
                            e.printStackTrace();
                            recordError = true;
                        } catch (Throwable e) { //TODO catch proper exception
                            System.out.println("ERROR THROWABLE EXCEPTION");
                            e.printStackTrace();
                            recordError = true;
                        }
                    }
                }
            }
        }, "addSamples Thread");

        //This thread is meant to increase the timer with the current time.
        timerThread = new Thread(new Runnable() {
            private Handler handler = new Handler();

            @Override
            public void run() {
                while((recorder.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING)) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            long currentTime = System.currentTimeMillis() - startTime;
                            defaultLanguage = Locale.getDefault();
                            String result = new SimpleDateFormat("mm:ss:SS", defaultLanguage)
                                    .format(new Date(currentTime));
                            timeText.setText(result);
                        }
                    });
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, "Timer Thread");

        recordingThread.start();
        addSamplesThread.start();
        timerThread.start();
    }

    /**
     * Stop the record and reset record features.
     */
    protected void stopRecording() {
        stopRecordButton.setVisibility(View.INVISIBLE);

        if (recorder != null) {
            recorder.stop();
            try {
                recordingThread.join();
                addSamplesThread.join();
                timerThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            recorder.release();
            recorder = null;
            recordExists = !recordError;
            recordingThread = null;
            addSamplesThread = null;
            startRecordButton.setVisibility(View.VISIBLE);

            String resultText = getResources().getString(R.string.recording_completed);
            if (recordError) {
                resultText = getResources().getString(R.string.recording_not_completed);
            }
            makeToast(resultText);


/*            try{

                //demoSpkRecSystem.addAndCreateSpeakerModel("test");
            } catch (AlizeException e) {
                e.printStackTrace();
            } catch (IdAlreadyExistsException e) {
                e.printStackTrace();
            } catch (Throwable e) { //TODO catch proper exception
                e.printStackTrace();
                makeToast(getResources().getString(R.string.recording_not_completed));
            }*/

            afterRecordProcessing();
        }
    }

    private void saveModel(){

        try{
            demoSpkRecSystem.addAndCreateSpeakerModel(txtName.getText().toString());
        } catch (AlizeException e){
            e.printStackTrace();
        } catch (IdAlreadyExistsException e){
            e.printStackTrace();
        }
    }

    /**
     * Method meant to be inherited and implemented with the post recording processes.
     */
    protected void afterRecordProcessing() {
        String result = "";
        resultText.setTextColor(Color.RED);

        if (recordError) {
            resultText.setText(result);
            makeToast(getResources().getString(R.string.recording_not_completed));
            return;
        }
        //Try to match a speaker with the record

        try {
            //Compare the audio signal with the speaker model we created earlier.
            String strSpeakerId = String.valueOf(speakerId);
            SimpleSpkDetSystem.SpkRecResult verificationResult = demoSpkRecSystem.verifySpeaker(speakerId);

            if (verificationResult.match) {
                result = "Vérification réussie vous êtes :\n" + demoSpkRecSystem.getSpeakerName(speakerId) + "\nSCORE:\n" + verificationResult.score;
                resultText.setTextColor(Color.GREEN);
                System.out.print("La vérif a réussie");
                Intent i = new Intent(getApplicationContext(), ManagePwd.class);
                i.putExtra("USERID", speakerId);
                i.putExtra("USERMAIL", speakerMail);
                startActivity(i);
                //btnConnexion.setVisibility(View.VISIBLE);
            }
            else {
                result = "Vérification a échoué\nSCORE:\n" + verificationResult.score;
                resultText.setTextColor(Color.RED);
            }
        }
        catch (AlizeException e) {
            e.printStackTrace();
            result = "Pas assez de son";
        } catch (Throwable e) { //TODO catch proper exception
            e.printStackTrace();
            result = "Pas assez de son" + e.getMessage();
        }
        resultText.setText(result);

        try {
            //Reset input, since we will not make any more use of this audio signal.
            demoSpkRecSystem.resetAudio();       //Reset the audio samples of the Alize system.
            demoSpkRecSystem.resetFeatures();    //Reset the features of the Alize system.
        } catch (AlizeException e) {
            e.printStackTrace();
        }
    }

    /**
     * Display the text specified on the bottom of the screen.
     * @param text Text you want to display.
     */
    protected void makeToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
