package com.example.jo_aq.biometrie_p8;

public class User {
    private String id;
    private String name;
    private String email;
    private String password;

    public User(){}

    public User(String id, String name, String email, String pwd){
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = pwd;
    }

    public void setId(String id){ this.id = id;}

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId(){ return id;}

    public String getName(){
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return ""+password;
    }

    @Override
    public String toString(){
        return "id:" + this.id + "nom: " + name + " email: " + email + " pwd: " + password;
    }
}
