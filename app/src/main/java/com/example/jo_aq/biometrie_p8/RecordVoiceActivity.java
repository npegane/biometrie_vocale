package com.example.jo_aq.biometrie_p8;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import AlizeSpkRec.AlizeException;
import AlizeSpkRec.IdAlreadyExistsException;

import static android.Manifest.permission.RECORD_AUDIO;

public class RecordVoiceActivity extends AppCompatActivity {

    protected static final int RECORDER_SAMPLERATE = 8000;
    protected static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    protected static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private static final String TXT_EXEMPLE = "La biométrie s’invite progressivement dans notre vie quotidienne, elle fait partie des grands enjeux pour un monde plus sûr. Le marché des produits d’authentification et d'identification est en pleine croissance, dû à la nécessité croissante du besoin de sécurité de chacun, dans les domaines privé, professionnel ou public.\n" +
            "\n" +
            "La biométrie est de plus en plus utilisée pour les titres d'identité, dans les aéroports, les établissements pénitentiaires, l'accès à des locaux sécurisés, le vote électronique, la sécurité des paiements bancaires ou des transactions via Internet.\n" +
            "\n" +
            "La biométrie est une alternative aux mots de passe et autres identifiants pour supprimer le doute sur l'identité. Elle permet de vérifier que l’usager est bien la personne qu’il prétend être." +
            "Une technologie établie, avec plusieurs technologies : empreintes digitales, visage 2D, Visage 3D, iris, rétine, voix, réseau veineux, forme de la main, comportemental (signature dynamique, frappe au clavier, navigation sur une tablette ou smartphone, façon de marcher).\n" +
            "\n" +
            "Pour un système d’authentification encore plus robuste, on peut associer simultanément plusieurs méthodes biométriques (multimodale).\n" +
            "\n" +
            "L'amélioration de la sécurité qui résulte du recours à la biométrie ne doit pas pour autant porter atteinte aux libertés individuelles.\n" +
            "\n" +
            "Les applications biométriques sont généralement associées à d’autres technologies de sécurité comme la carte à puce, le cryptage, l’anonymat des données stockées...";

    protected int bufferElements2Rec = 2000;
    protected int bytesPerElement = 2; // 2 bytes in 16bit format
    protected long startTime;
    protected TextView timeText;
    protected SpkRecSys demoSpkRecSystem;
    protected boolean emptyRecord = false;
    protected boolean recordExists = false;
    protected boolean recordError = false;
    protected AudioRecord recorder = null;
    protected Button startRecordButton, stopRecordButton, btnSaveModel;
    protected Thread recordingThread = null, addSamplesThread = null, timerThread = null;
    protected Locale defaultLanguage;
    protected TextView txtRecord;
    private String name;
    private String email;
    private String password;
    private User user;
    MyDataBase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_voice);
        Bundle extras = getIntent().getExtras();

        name = extras.getString("USERNAME");
        email = extras.getString("EMAIL");
        password = extras.getString("PASSWORD");
        System.out.println("NOM: "+name+", EMAIL: "+email+", PASSWORD: "+password);


        try {
            //Initialize the SpeakerRecognition system and get the singleton instance.
            demoSpkRecSystem = SpkRecSys.getSharedInstance(this);
        }
        catch (AlizeException | IOException e) {
            e.printStackTrace();
        }
        startRecordButton = findViewById(R.id.startRecordButton);
        stopRecordButton = findViewById(R.id.stopRecordButton);
        btnSaveModel = findViewById(R.id.btnSaveModel);
        timeText = findViewById(R.id.timeText);
        txtRecord = findViewById(R.id.txtRecord);
        txtRecord.setText(TXT_EXEMPLE);
        startRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRecording();
            }
        });

        stopRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecording();
            }
        });

        btnSaveModel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveModel();
            }
        });
    }

    protected boolean checkPermission() {
        return ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED;
    }

    protected void requestPermission() {
        ActivityCompat.requestPermissions(RecordVoiceActivity.this, new
                String[]{RECORD_AUDIO}, 42);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 42: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startRecording();
                }
                else {
                    //makeToast("est");
                    System.out.println("ERREUR PERMISSION");
                }
                break;
            }
        }
    }

    public void signup() {
        Log.d("TAG", "Signup");
        db = new MyDataBase(this);
        //_recordButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(RecordVoiceActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        // TODO: Implement your own signup logic here.
        db.insertUser(user);
        Log.d("TAG", user.toString());

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        User userFromDB = db.getUser(email);
                        Log.d("TAG", userFromDB.toString());
                        if(userFromDB != null){
                            onSignupSuccess();
                            Log.d("TAG",user.getPassword()+ "  pffff:  " + userFromDB.getPassword());
                            //db.close();
                        }else{
                            onSignupFailed();
                        }
                        db.close();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }

    public void onSignupSuccess() {
        Log.d("TAG", "onSignupSuccess called");
        //_recordButton.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    public void onSignupFailed() {
        Log.d("TAG", "onSignupSuccess called");
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        //_recordButton.setEnabled(true);
    }

    protected void startRecording() {
        if (!checkPermission()) {
            requestPermission();
            return;
        }

        emptyRecord = true;
        recordError = false;
        startRecordButton.setVisibility(View.INVISIBLE);
        stopRecordButton.setVisibility(View.VISIBLE);
        timeText.setText("00:00:00");

        //Start the record
        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING, bufferElements2Rec * bytesPerElement);
        recorder.startRecording();

        if (recordExists) {
            try {
                //Reset input, since we will not make any more use of this audio signal.
                demoSpkRecSystem.resetAudio();       //Reset the audio samples of the Alize system.
                demoSpkRecSystem.resetFeatures();    //Reset the features of the Alize system.
            } catch (AlizeException e) {
                e.printStackTrace();
            }
            recordExists = false;
        }

        final List<short[]> audioPackets = Collections.synchronizedList(new ArrayList<short[]>());

        //This thread is meant to record the audio samples with android recorder.
        recordingThread = new Thread(new Runnable() {
            private Handler handler = new Handler();

            @Override
            public void run() {
                startTime = System.currentTimeMillis();

                short[] tmpAudioSamples = new short[bufferElements2Rec];
                while (recorder.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
                    int samplesRead = recorder.read(tmpAudioSamples, 0, bufferElements2Rec);

                    if (samplesRead > 0) {
                        short[] samples = new short[samplesRead];
                        //System.arraycopy(tmpAudioSamples, 0, samples, 0, samplesRead);
                        for (int i=0; i < samples.length; i++) {
                            samples[i] = tmpAudioSamples[i];
                            if (samples[i] != 0) {
                                emptyRecord = false;
                            }
                        }

                        synchronized (audioPackets) {
                            audioPackets.add(samples);
                        }
                    }
                }
            }
        }, "AudioRecorder Thread");

        //This thread is meant to use the audio samples and send them to the Alize system.
        addSamplesThread = new Thread(new Runnable() {
            private Handler handler = new Handler();

            @Override
            public void run() {
                short[] nextElement;

                while ((recorder.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING)
                        || (!audioPackets.isEmpty())) {
                    nextElement = null;

                    synchronized (audioPackets) {
                        if (!audioPackets.isEmpty()) {
                            nextElement = audioPackets.get(0);
                            audioPackets.remove(0);
                        }
                    }
                    if (nextElement != null) {
                        try {
                            //Receive an audio signal as 16-bit signed integer linear PCM, parameterize it and add it to the feature server.
                            demoSpkRecSystem.addAudio(nextElement);
                        } catch (AlizeException e) {
                            e.printStackTrace();
                            recordError = true;
                        } catch (Throwable e) { //TODO catch proper exception
                            e.printStackTrace();
                            recordError = true;
                        }
                    }
                }

                try {
                    recordingThread.join(); //Wait the recordingThread to end.
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                while (!audioPackets.isEmpty()) {
                    nextElement = audioPackets.get(0);
                    audioPackets.remove(0);

                    if (nextElement != null) {
                        try {
                            demoSpkRecSystem.addAudio(nextElement);
                        } catch (AlizeException e) {
                            e.printStackTrace();
                            recordError = true;
                        } catch (Throwable e) { //TODO catch proper exception
                            e.printStackTrace();
                            recordError = true;
                        }
                    }
                }
            }
        }, "addSamples Thread");

        //This thread is meant to increase the timer with the current time.
        timerThread = new Thread(new Runnable() {
            private Handler handler = new Handler();

            @Override
            public void run() {
                while((recorder.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING)) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            long currentTime = System.currentTimeMillis() - startTime;
                            defaultLanguage = Locale.getDefault();
                            String result = new SimpleDateFormat("mm:ss:SS", defaultLanguage)
                                    .format(new Date(currentTime));
                            timeText.setText(result);
                        }
                    });
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, "Timer Thread");

        recordingThread.start();
        addSamplesThread.start();
        timerThread.start();
    }

    /**
     * Stop the record and reset record features.
     */
    protected void stopRecording() {
        stopRecordButton.setVisibility(View.INVISIBLE);

        if (recorder != null) {
            recorder.stop();
            try {
                recordingThread.join();
                addSamplesThread.join();
                timerThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            recorder.release();
            recorder = null;
            recordExists = !recordError;
            recordingThread = null;
            addSamplesThread = null;
            startRecordButton.setVisibility(View.VISIBLE);

            String resultText = getResources().getString(R.string.recording_completed);
            if (recordError) {
                resultText = getResources().getString(R.string.recording_not_completed);
            }
            makeToast(resultText);
            btnSaveModel.setVisibility(View.VISIBLE);

           try{
               demoSpkRecSystem.resetAudio();
               demoSpkRecSystem.resetFeatures();
               //demoSpkRecSystem.addAndCreateSpeakerModel("test");
            } catch (AlizeException e) {
                e.printStackTrace();
            } catch (Throwable e) { //TODO catch proper exception
                e.printStackTrace();
                makeToast(getResources().getString(R.string.recording_not_completed));
            }

            afterRecordProcessing();
        }
    }

    private void saveModel(){

        try{
            String id = demoSpkRecSystem.addAndCreateSpeakerModel(name);
            Log.d("SAVEMODEL ID", "id:"+id);
            user = new User();
            user.setId(id);
            user.setPassword(password);
            user.setName(name);
            user.setEmail(email);
            signup();
            System.out.println(db.getAllUser().size());
            demoSpkRecSystem.resetAudio();
            demoSpkRecSystem.resetFeatures();
        } catch (AlizeException e){
            e.printStackTrace();
        } catch (IdAlreadyExistsException e){
            e.printStackTrace();
        }

    }

    /**
     * Method meant to be inherited and implemented with the post recording processes.
     */
    protected void afterRecordProcessing() {}

    /**
     * Display the text specified on the bottom of the screen.
     * @param text Text you want to display.
     */
    protected void makeToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }


}
