package com.example.jo_aq.biometrie_p8;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ManagePwd extends AppCompatActivity {

    private static final String TAG = "ManagePasswords";

    int idRowAccount[];

    MyDataBase db;
    User user;
    AccountManager user_Account;

    @BindView(R.id.user_connected)
    TextView _userConnect;
    @BindView(R.id.addAccount)
    FloatingActionButton _addAccount;
    @BindView(R.id.listAccount)
    TableLayout _listAccount;

    Button _saveAccount;
    Button _cancelDialog;
    EditText _newApp;
    EditText _newLoginApp;
    EditText _newPwdApp;

    private static final int REQUEST_LOGIN = 0;
    private int nbAccount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_pwd);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();

        String userid = extras.getString("USERID");
        String userMail = extras.getString("USERMAIL");

        System.out.println(userMail + "LAAAAAAAAAAAAAAAAAAA");
        loadList(userMail);
        db = new MyDataBase(this);
        _listAccount.setClickable(true);

        AlertDialog.Builder bluider = new AlertDialog.Builder(ManagePwd.this, R.style.AppTheme);
        LayoutInflater inflater = getLayoutInflater();

        View dialogAccount = inflater.inflate(R.layout.dialog_account, null);

        _saveAccount = (Button)dialogAccount.findViewById(R.id.btn_addAccount);
        _cancelDialog = (Button)dialogAccount.findViewById(R.id.btn_cancel);
        _newApp = (EditText)dialogAccount.findViewById(R.id.input_appName);
        _newLoginApp = (EditText)dialogAccount.findViewById(R.id.input_loginAccount);
        _newPwdApp = (EditText)dialogAccount.findViewById(R.id.input_pwdAccount);

        bluider.setCancelable(false);
        bluider.setView(dialogAccount);

        final AlertDialog dialog = bluider.create();

        _addAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newAccountUser(dialog);
            }
        });
    }
    public void newAccountUser(final AlertDialog dialog){
        Log.d(TAG, "newAccountUser");
        dialog.show();

        _saveAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validate()) {
                    return;
                }

                _saveAccount.setEnabled(false);
                String password = _newPwdApp.getText().toString();
                String login = _newLoginApp.getText().toString();
                String nameApp = _newApp.getText().toString();

                user_Account = new AccountManager(user.getId(), nameApp, login, password);
                db.insertAccount(user_Account);
                updateList(nameApp, login, password);
                _saveAccount.setEnabled(true);
                _newPwdApp.setText("");
                _newLoginApp.setText("");
                _newApp.setText("");
                dialog.dismiss();
                db.close();
            }
        });

        _cancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _saveAccount.setEnabled(true);
                _newPwdApp.setText("");
                _newLoginApp.setText("");
                _newApp.setText("");
                dialog.dismiss();
            }
        });

    }

    public boolean validate(){
        Log.d(TAG, "validate");
        boolean valid = true;

        String password = _newPwdApp.getText().toString();
        String login = _newLoginApp.getText().toString();
        String nameApp = _newApp.getText().toString();

        if (nameApp.isEmpty()) {
            _newApp.setError("required field");
            valid = false;
        } else {
            _newApp.setError(null);
        }

        if (login.isEmpty()) {
            _newLoginApp.setError("required field");
            valid = false;
        } else {
            _newLoginApp.setError(null);
        }

        if (password.isEmpty() || password.length() < 4) {
            _newPwdApp.setError("upper or equal 4 alphanumeric characters");
            valid = false;
        } else {
            _newPwdApp.setError(null);
        }

        return valid;
    }

    protected void loadList(String userId) {
        Log.d(TAG, "onActivityResult onActivityResult called");
        Log.i(TAG, "onActivityResult RESULT VALID");
        db = new MyDataBase(this);
        user = db.getUser(userId);
        _userConnect.setText(Html.fromHtml("<b><h1>WELCOME "+user.getName().toUpperCase()+"</h1></b>"));

        List<AccountManager> accounts = db.getAccountUser(user.getId());

        for(AccountManager account: accounts) {
            updateList(account.getName_app(), account.getLogin_app(), account.getPwd_app());
        }
        db.close();
    }

    public void updateList(String app, String login, String pwd){


        TableRow tableRow = new TableRow(this);
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        tableRow.setLayoutParams(layoutParams);

//        idRowAccount[nbAccount] = tableRow.getId();
//        nbAccount++;

        tableRow.setGravity(Gravity.CENTER);
        tableRow.setClickable(true);
        tableRow.setPadding(0, 0, 0, 100);

        TextView textApp = new TextView(this);
        TextView textLogin = new TextView(this);
        final TextView textPwd = new TextView(this);

        textApp.setPadding(5, 0, 20, 0);
        textLogin.setPadding(20, 0, 20, 0);
        textPwd.setPadding(20, 0, 5, 0);

        textApp.setGravity(Gravity.CENTER);
        textLogin.setGravity(Gravity.CENTER);
        textPwd.setGravity(Gravity.CENTER);

//        textPwd.setTransformationMethod(new PasswordTransformationMethod());

        textApp.setTextAppearance(this, R.style.TextAppearance_AppCompat_Medium);
        textLogin.setTextAppearance(this, R.style.TextAppearance_AppCompat_Medium);
        textPwd.setTextAppearance(this, R.style.TextAppearance_AppCompat_Medium);

        textApp.setText(Html.fromHtml("<b>"+app.toUpperCase()+"</b><br/>"));
        textLogin.setText(Html.fromHtml("<b>Login</b><br/>"+login));
        textPwd.setText(Html.fromHtml("<br/>"+pwd));

//        textPwd.setOnClickListener(new View.OnClickListener() {
//            @SuppressLint("ResourceAsColor")
//            @Override
//            public void onClick(View v) {
//                v.setBackgroundColor(R.color.primary_darker);
//                textPwd.setTransformationMethod(null);
//                try {
//                    Thread.sleep(2000); //1000 milliseconds is one second.
//                }
//                catch (InterruptedException e)
//                {
//                    e.printStackTrace();
//                }
//_                wait(2000);
//                textPwd.setTransformationMethod(new PasswordTransformationMethod());
//            }
//        });
//        textPwd.setTransformationMethod(null);
        tableRow.addView(textApp, 0);
        tableRow.addView(textLogin, 1);
        tableRow.addView(textPwd, 2);

        _listAccount.addView(tableRow);
        _listAccount.setClickable(true);
    }
}
