package com.example.jo_aq.biometrie_p8;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.jo_aq.biometrie_p8.speakerslist.SpeakerListAdapter;
import com.example.jo_aq.biometrie_p8.speakerslist.Speaker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import AlizeSpkRec.AlizeException;

public class ListModelActivity extends AppCompatActivity {
    private String[] speakers;
    private SpeakerListAdapter adapter;
    private Button btnReco;
    protected SpkRecSys demoSpkRecSystem;
    private MyDataBase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_model);
        System.out.println("ONCREATE");
        adapter = new SpeakerListAdapter(ListModelActivity.this,R.layout.list_item, new ArrayList<Speaker>());
        ListView listModel = findViewById(R.id.listModel);
        listModel.setAdapter(adapter);
        btnReco = findViewById(R.id.btnReco);
        btnReco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ReconActivity.class);
                startActivity(intent);
            }
        });
        try {
            demoSpkRecSystem = SpkRecSys.getSharedInstance(this);
            speakers = demoSpkRecSystem.speakerIDs();
            System.out.println("SPEAKERS");
            System.out.println(speakers.length);
            System.out.println("IN DATABASE");
            db = new MyDataBase(this);
            List<User> list = db.getAllUser();
            System.out.println("LIIIIIIIIIIIIIIST");
            if(list.size()>0){
                System.out.println(list.get(0).getId());
                System.out.println(list.get(0).getName());
                System.out.println(list.get(0).getEmail());
                System.out.println(list.get(0).getPassword());
            }

            db.close();
            clearAndFill();
        }
        catch(AlizeException e){
            e.printStackTrace();
        }
        catch(IOException e){
            e.printStackTrace();
        }

    }

    private void clearAndFill() throws AlizeException {
        updateSpeakersListObject();
        if (speakers.length == 0) {
            return;
        }
        adapter.clear();
        for (String speakerId : speakers) {
            adapter.insert(new Speaker(speakerId, demoSpkRecSystem.getSpeakerName(speakerId)), adapter.getCount());
        }
    }

    private void updateSpeakersListObject() throws AlizeException {
        speakers = demoSpkRecSystem.speakerIDs(); //Get all speakers ids.
    }

    public void removeSpeakerOnClickHandler(View v) {
        Speaker itemToRemove = (Speaker)v.getTag();
        String speakerId = itemToRemove.getId();
        System.out.println(speakerId);

        try {
            if (!speakerId.isEmpty() && !itemToRemove.getName().isEmpty()) {
                //Remove the speaker speakerId from the Alize system.
                db = new MyDataBase(this);
                db.deleteUserById(speakerId);
                db.close();
                demoSpkRecSystem.removeSpeaker(speakerId);
            }
            updateSpeakersListObject();

        } catch (AlizeException e) {
            e.printStackTrace();
            Toast.makeText(ListModelActivity.this, "Impossible de supprimer", Toast.LENGTH_SHORT).show();
        }
        adapter.remove(itemToRemove);
        updateListViewContent();
    }

    private void updateListViewContent() {
        if (speakers.length == 0) {
            if (adapter.getCount() == 0) {
                //noSpeakers.setVisibility(View.VISIBLE);
                btnReco.setEnabled(false);
                //removeAll.setEnabled(false);
            }
            //else
                //noSpeakers.setVisibility(View.INVISIBLE);
        }
        else {
            //noSpeakers.setVisibility(View.INVISIBLE);
            //identifyButton.setEnabled(true);
            //removeAll.setEnabled(true);
            btnReco.setEnabled(true);
        }
    }
}
