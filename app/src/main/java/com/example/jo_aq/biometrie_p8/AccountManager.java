package com.example.jo_aq.biometrie_p8;

public class AccountManager {
    private String id;
    private String id_user;
    private String name_app;
    private String login_app;
    private String pwd_app;

    public AccountManager() {
    }

    public AccountManager(String id_user, String name_app, String login_app, String pwd_app) {
        this.id_user = id_user;
        this.name_app = name_app;
        this.login_app = login_app;
        this.pwd_app = pwd_app;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getName_app() {
        return name_app;
    }

    public void setName_app(String name_app) {
        this.name_app = name_app;
    }

    public String getLogin_app() {
        return login_app;
    }

    public void setLogin_app(String login_app) {
        this.login_app = login_app;
    }

    public String getPwd_app() {
        return pwd_app;
    }

    public void setPwd_app(String pwd_app) {
        this.pwd_app = pwd_app;
    }

}

