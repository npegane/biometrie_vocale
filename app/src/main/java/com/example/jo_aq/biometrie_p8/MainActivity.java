package com.example.jo_aq.biometrie_p8;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    MyDataBase db;
    TextView allUsers;
    private static final int REQUEST_LOGIN = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        allUsers = findViewById(R.id.user_connected);

        Intent intent = new Intent(this, Login.class);
        startActivityForResult(intent, REQUEST_LOGIN);

        //db.deleteAllUser();
//        User test = new User("Renan", "Banger@gmail.com", "12345");
//        db.insertUser(test);
//        List<User> users = db.getAllUser();
//        for(User user : users){
//            allUsers.append(user.toString() +  "\n\n");
//        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult onActivityResult called");
        if (requestCode == REQUEST_LOGIN) {
            Log.d(TAG, "onActivityResult REQUEST VALID");
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "onActivityResult RESULT VALID");
                db = new MyDataBase(this);
                List<User> users = db.getAllUser();
                for(User user : users){
                    allUsers.append(user.toString() +  "\n\n");
                }
                //db.close();
            }
        }
    }
}
