package com.example.jo_aq.biometrie_p8;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Login extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    private Button test, record;
    //@BindView(R.id.input_email)
    //EditText _emailText;
    //@BindView(R.id.input_password) EditText _passwordText;
//    @BindView(R.id.btn_login)
//    Button _loginButton;
    //@BindView(R.id.link_signup)
    TextView _signupLink;

    User user;
    MyDataBase db;
    List<User> listeUtil;
    private LinearLayout scrollLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        test = findViewById(R.id.test);
        record = findViewById(R.id.btnRecord);
        db = new MyDataBase(this);
        listeUtil = db.getAllUser();
        db.close();
        scrollLayout = (LinearLayout) findViewById(R.id.scrollLayout);

        for(int i = 0; i < listeUtil.size(); i++){
            Button btn = new Button(this);
            btn.setText(listeUtil.get(i).getName());
            final String speakerId = listeUtil.get(i).getId();
            final String speakerMail = listeUtil.get(i).getEmail();

            //Log.i("LOGIN", "SPEAKER:"+db.getUser(speakerId).getId());
            scrollLayout.addView(btn);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), VerifyActivity.class);
                    i.putExtra("SPEAKERID", speakerId);
                    i.putExtra("SPEAKERMAIL", speakerMail);
                    startActivity(i);
                }
            });
        }
        //System.out.println(listeUtil.toString()+", \n nb Util:"+ listeUtil.size());

        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ListModelActivity.class);
                startActivity(intent);
            }
        });
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Signup.class);
                startActivity(intent);
            }
        });


        /*_loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });*/

        /*_signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), Signup.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });*/
    }

   /* public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        //_loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(Login.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();
        //db.insertUser(user);

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        user = db.getUser(email);
                        Log.d(TAG, user.toString());
                        if(user != null && user.getPassword().equals(password)){
                            onLoginSuccess();
                            Log.d(TAG, "Connection reussi !");
                            //db.close();
                        }else{
                            onLoginFailed();
                            Log.d(TAG, "Connection failed !");
                            Log.d(TAG,user.getPassword()+ "  pffff:  " + password);
                        }
                        progressDialog.dismiss();
                    }
                }, 3000);
    }*/


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult called");
        if (requestCode == REQUEST_SIGNUP) {
            Log.d(TAG, "onActivityResult REQUEST VALID");
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "onActivityResult RESULT VALID");
                setResult(RESULT_OK, null);
                //db.close();
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(false);
    }

    public void onLoginSuccess() {
        Log.d(TAG, "onLoginSuccess called");
        //_loginButton.setEnabled(true);
        setResult(RESULT_OK, null);
        //db.close();
        finish();
    }

    public void onLoginFailed() {
        Log.d(TAG, "onLoginFailed called");
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        //_loginButton.setEnabled(true);
    }

    /*public boolean validate() {
        Log.d(TAG, "validate called");
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 25) {
            _passwordText.setError("between 4 and 25 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }*/
}
